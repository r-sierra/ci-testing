stages:
  - build
  - test

default:
  image: ruby:2.5.8
  cache:
    key:
      files:
        - Gemfile.lock
      prefix: $CI_COMMIT_REF_SLUG
    paths:
      - vendor/ruby
    policy: pull
  before_script:
    - ruby -v
    - which ruby
    - gem install bundler --no-document
    - bundle version
    - bundle config path vendor
    - bundle install --jobs=$(nproc) --retry=3 --quiet && bundle check
    - if [ -n "$TRIGGERED_USER" ] && [ -n "$TRIGGER_SOURCE" ]; then
        echo "Pipeline triggered by $TRIGGERED_USER at $TRIGGER_SOURCE";
      fi
    - export LANG=C.UTF-8

workflow:
  # Based on https://gitlab.com/gitlab-org/gitlab-qa/-/blob/master/.gitlab-ci.yml#L28
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For the default branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'
    # For triggers from GitLab MR pipelines (and pipelines from other projects), create a pipeline
    - if: '$CI_PIPELINE_SOURCE == "pipeline"'
    # When using Run pipeline button in the GitLab UI, from the project’s CI/CD > Pipelines section, create a pipeline.
    - if: '$CI_PIPELINE_SOURCE == "web"'

include:
  - template: Code-Quality.gitlab-ci.yml

code_quality:
  # Do not run default:before_script
  before_script: []
  variables:
    REPORT_FORMAT: html
  artifacts:
    paths: [gl-code-quality-report.html]
  rules:
    - if: '$CODE_QUALITY_DISABLED'
      when: never
    # Run code quality job in merge request pipelines
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    # Run code quality job in pipelines on the default branch (but not in other branch pipelines)
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # Run code quality job in pipelines for tags
    - if: '$CI_COMMIT_TAG'

.base_db:
  services:
    - mariadb:latest
  variables:
    MARIADB_DATABASE: db_test
    MARIADB_USER: ci_test_user
    MARIADB_PASSWORD: db_password
    MARIADB_RANDOM_ROOT_PASSWORD: random
    DATABASE_HOST: mariadb
    DATABASE_USER: ci_test_user
    DATABASE_PASSWORD: db_password

build:
  stage: build
  cache:
    policy: pull-push
  script:
    - echo 'Build done. All gems were installed!'

security:brakeman:
  stage: test
  allow_failure: true
  script:
    - bundle exec brakeman

security:bundler-audit:
  stage: test
  allow_failure: true
  script:
    - bundle exec bundler-audit

test:rspec:
  stage: test
  extends: .base_db
  script:
    - apt-get update -qq && apt-get install -y -qq nodejs
    - bundle exec rake db:setup RAILS_ENV=test
    - bundle exec rspec
