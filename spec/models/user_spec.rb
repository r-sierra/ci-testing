require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to validate_presence_of :name }
  it { is_expected.to validate_presence_of :email }
  it { is_expected.to validate_presence_of :username }
  it { is_expected.to validate_length_of(:name).is_at_most(50) }
  it { is_expected.to validate_length_of(:username).is_at_most(50) }
  it { is_expected.to have_db_index(:username).unique }
  it { is_expected.to have_db_index(:email).unique }
end
